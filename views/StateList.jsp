<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
	String contextPath= request.getContextPath();
	%>
    <%@ page isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <a href="<%=contextPath%>/views/StateForm.jsp"> ADD NEW STATE</a>
	<table border="2" width="70%" cellpadding="2">
         <tr>
         	<th>Id</th>
            <th>Name</th>
         </tr>
	    <c:forEach var="lis" items="${StateList}">
	        <tr>
	            <td>${lis.id}</td>
	            <td>${lis.stateName}</td>
	        </tr>
	    </c:forEach>
	</table>
</body>
</html>